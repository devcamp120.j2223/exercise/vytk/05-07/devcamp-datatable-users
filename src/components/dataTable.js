import * as React from "react";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Button,
  Pagination,
  Modal,
  FormLabel,
  TextareaAutosize,
  FormControl,
  MenuItem,
  Box,
  InputLabel,
  Select
} from "@mui/material";
import { useEffect, useState } from "react";
export default function DataTable() {
  const [row, setRows] = useState([]);

  //Limit: Số lượng bản ghi trên 1 trang
  //const limit = 10;
  const [limit, setLimit] = useState(10);
  const handleChange = (event) => {
    console.log(event.target.value)
    setLimit(event.target.value);
  };

  //Số trang: Tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
  const [countPage, setcountPage] = useState(0);

  //Trang hiện tại:
  const [page, setPage] = useState(1);

  //Modal
  const [openModal, setOpenModal] = React.useState(false);
  const [modal, setModal] = useState([]);
  const handleOpen = () => setOpenModal(true);
  const handleClose = () => setOpenModal(false);

  const onChangePagination = (event, value) => {
    setPage(value);
  };
  const callAPI = async (url) => {
    const res = await fetch(url);
    const data = await res.json();

    return data;
  };
  const clickHandler = (data) => {
    console.log(data);
    handleOpen();
    setModal(data);
  };

  useEffect(() => {
    callAPI("http://42.115.221.44:8080/devcamp-register-java-api/users")
      .then((data) => {
        setcountPage(Math.ceil(data.length / limit));
        setRows(data.slice((page - 1) * limit, page * limit));
      })
      .catch((err) => {
        console.error(err.message);
      });
  }, [page,limit]);
  return (
    <Container>
      <Grid justify-content="center">
        <Box sx={{ minWidth: 120 }} mt={5}>
          <FormControl>
            <InputLabel >Entries</InputLabel>
            <Select
              value={limit}
              label="Age"
              onChange={handleChange}
              style={{padding:'10px 10px'}}
            >
              <MenuItem value={10}>10</MenuItem>
              <MenuItem value={20}>20</MenuItem>
              <MenuItem value={30}>30</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Grid item mt={3}>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Mã người dùng</TableCell>
                  <TableCell align="center">Firstname</TableCell>
                  <TableCell align="center">Lastname</TableCell>
                  <TableCell align="center">Country</TableCell>
                  <TableCell align="center">Subject</TableCell>
                  <TableCell align="center">Custom Type</TableCell>
                  <TableCell align="center">Register Status</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {row.map((row, index) => {
                  return (
                    <TableRow key={index}>
                      <TableCell align="center" component="th" scope="row">
                        {row.id}
                      </TableCell>
                      <TableCell align="center">{row.firstname}</TableCell>
                      <TableCell align="center">{row.lastname}</TableCell>
                      <TableCell align="center">{row.country}</TableCell>
                      <TableCell align="center">{row.subject}</TableCell>
                      <TableCell align="center">{row.customerType}</TableCell>
                      <TableCell align="center">{row.registerStatus}</TableCell>
                      <TableCell align="center">
                        <Button
                          variant="contained"
                          onClick={() => {
                            clickHandler(row);
                          }}
                        >
                          Chi Tiết
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
      <Grid container mt={3} mb={3} justifyContent="flex-end">
        <Grid item>
          <Pagination
            count={countPage}
            defaultPage={page}
            onChange={onChangePagination}
          />
        </Grid>
      </Grid>
      <Modal open={openModal} onClose={handleClose}>
        <form
          align="center"
          style={{ backgroundColor: "white", margin: "200px" }}
        >
          <Grid
            container
            justify-content="center"
            alignItems="center"
            direction="column"
          >
            <h2>Infomation User</h2>
            <Grid container direction="row" mt={1}>
              <Grid item xs={3} textAlign="right">
                <FormLabel>ID</FormLabel>
              </Grid>
              <Grid item xs={7}>
                <TextareaAutosize
                  style={{ width: "90%" }}
                  defaultValue={modal.id}
                ></TextareaAutosize>
              </Grid>
            </Grid>
            <Grid container direction="row">
              <Grid item xs={3} textAlign="right">
                <FormLabel>Firstname</FormLabel>
              </Grid>
              <Grid item xs={7}>
                <TextareaAutosize
                  style={{ width: "90%" }}
                  defaultValue={modal.firstname}
                ></TextareaAutosize>
              </Grid>
            </Grid>
            <Grid container direction="row">
              <Grid item xs={3} textAlign="right">
                <FormLabel>Lastname</FormLabel>
              </Grid>
              <Grid item xs={7}>
                <TextareaAutosize
                  style={{ width: "90%" }}
                  defaultValue={modal.lastname}
                ></TextareaAutosize>
              </Grid>
            </Grid>
            <Grid container direction="row">
              <Grid item xs={3} textAlign="right">
                <FormLabel>Country</FormLabel>
              </Grid>
              <Grid item xs={7}>
                <TextareaAutosize
                  style={{ width: "90%" }}
                  defaultValue={modal.country}
                ></TextareaAutosize>
              </Grid>
            </Grid>
            <Grid container direction="row">
              <Grid item xs={3} textAlign="right">
                <FormLabel>Subject</FormLabel>
              </Grid>
              <Grid item xs={7}>
                <TextareaAutosize
                  style={{ width: "90%" }}
                  defaultValue={modal.subject}
                ></TextareaAutosize>
              </Grid>
            </Grid>
            <Grid container direction="row">
              <Grid item xs={3} textAlign="right">
                <FormLabel>Custom Type</FormLabel>
              </Grid>
              <Grid item xs={7}>
                <TextareaAutosize
                  style={{ width: "90%" }}
                  defaultValue={modal.customerType}
                ></TextareaAutosize>
              </Grid>
            </Grid>
            <Grid container direction="row" mb={3}>
              <Grid item xs={3} textAlign="right">
                <FormLabel>Register Status</FormLabel>
              </Grid>
              <Grid item xs={7}>
                <TextareaAutosize
                  style={{ width: "90%" }}
                  defaultValue={modal.registerStatus}
                ></TextareaAutosize>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </Modal>
    </Container>
  );
}
